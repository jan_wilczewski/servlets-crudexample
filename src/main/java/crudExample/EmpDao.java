package crudExample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class EmpDao {

    public static Connection getConnection(){
        Connection con = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","oracle");
        }catch (Exception e){
            System.out.println(e);
        }
        return con;
    }

    public static int save(Emp employee){
        int status = 0;
        try {
            Connection con = EmpDao.getConnection();
            PreparedStatement ps = con.prepareStatement(
                    "INSERT INTO user905(name, password, email, country) VALUES (?,?,?,?)");

            ps.setString(1, employee.getName());
            ps.setString(2, employee.getPassword());
            ps.setString(3, employee.getEmail());
            ps.setString(4, employee.getCountry());

            status = ps.executeUpdate();

            con.close();

        }catch (Exception ex){
            ex.printStackTrace();
        }
        return status;
    }

    public static int update(Emp employee){

        int status = 0;
        try {
            Connection con = EmpDao.getConnection();
            PreparedStatement ps = con.prepareStatement(
                    "UPDATE user905 SET name=?,password=?,email=?,country=? WHERE id=?");

            ps.setString(1, employee.getName());
            ps.setString(2, employee.getPassword());
            ps.setString(3, employee.getEmail());
            ps.setString(4, employee.getCountry());
            ps.setInt(5, employee.getId());
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return status;
    }

    public static int delete(int id){
        int status = 0;

        try {
            Connection con = EmpDao.getConnection();
            PreparedStatement ps = con.prepareStatement("DELETE FROM user905 WHERE id = ?");
            ps.setInt(1, id);
            status = ps.executeUpdate();

            con.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return status;
    }

    public static Emp getEmployeeById (int id) {
        Emp employee = new Emp();

        try {
            Connection con = EmpDao.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM user905 where id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()){
                employee.setId(rs.getInt(1));
                employee.setName(rs.getString(2));
                employee.setPassword(rs.getString(3));
                employee.setEmail(rs.getString(4));
                employee.setCountry(rs.getString(5));
            }
            con.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return employee;
    }

    public static List<Emp> getAllEmployees(){
        List<Emp> list = new ArrayList<>();

        try {
            Connection con = EmpDao.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM user905");
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                Emp employee = new Emp();
                employee.setId(rs.getInt(1));
                employee.setName(rs.getString(2));
                employee.setPassword(rs.getString(3));
                employee.setEmail(rs.getString(4));
                employee.setCountry(rs.getString(5));
            }

            con.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }
}
